package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SongCowriterRoleId implements Serializable {
    private Long songCowriterSeq;
    private String roleCd;
}
