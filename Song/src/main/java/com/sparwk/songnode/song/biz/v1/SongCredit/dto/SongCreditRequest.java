package com.sparwk.songnode.song.biz.v1.SongCredit.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCreditRequest {

    @Schema(description = "참여할 song Id")
    private Long songId;
    @Schema(description = "협업자 프로필 아이디 리스트")
    private String profileList;

}
