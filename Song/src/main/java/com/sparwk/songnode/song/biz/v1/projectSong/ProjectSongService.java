package com.sparwk.songnode.song.biz.v1.projectSong;

import com.sparwk.songnode.song.biz.v1.projectSong.dto.ProjectSongCountResponse;
import com.sparwk.songnode.song.jpa.repository.ProjectSongRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectSongService {

    private final ProjectSongRepository projectSongRepository;

    private final HttpServletRequest request;
    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(ProjectSongService.class);

    /**
     *
     * @param projId
     * @return
     */
    public List<ProjectSongCountResponse> findProjectSongCount(Long projId) {
        logger.info("#### findProjectSongCount SERVICE :: IN <<<< {}", projId);
        List<ProjectSongCountResponse> result = projectSongRepository.findProjIdAndProfileIdAndSongCntByProjId(projId);
        logger.debug("##### RESULT  IN <<<<< {} ", result);
        return result;
    }
}
