package com.sparwk.songnode.song.config.filter.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;

@Service
public class TokenDecoding {

    @Value("${security.jwt.token.secret-key}")
    private String jwt;

    @Value("${security.jwt.token.secret-key}")
    private String secretKey;

    public boolean isSigned(String token) {
        return Jwts.parserBuilder()
                .setSigningKey(secretKey.getBytes())
                .build()
                .isSigned(token);
    }

    public String getSubject(String token) {
        Claims claim = Jwts.parserBuilder()
                .setSigningKey(DatatypeConverter.parseBase64Binary(jwt))
                .build()
                .parseClaimsJws(token)
                .getBody();
        return claim.getSubject();
    }

    public UserInfoDTO decode(){

        return null;
    }



//    public WpayUser decode(String token) {
//        Claims claims = Jwts.parserBuilder()
//                .setSigningKey(key)
//                .build()
//                .parseClaimsJws(token)
//                .getBody();
//
//
//        return WpayUser.builder()
//                .mgtId((String)claims.get("mgtId"))
//                .mmbrId((String)claims.get("mmbrId"))
//                .mmbrNm((String)claims.get("mmbrNm"))
//                .oauthTpCd((String)claims.get("oauthTpCd"))
//                .borno((String)claims.get("borno"))
//                .birthdt((String)claims.get("birthdt"))
//                .mpno((String)claims.get("mpno"))
//                .email((String)claims.get("email"))
////                .dfpmServiceUseYn((String)claims.get("dfpmServiceUseYn"))
//                .dfpmUseYn((String)claims.get("dfpmUseYn"))
//                .orgnmCtfctnUseYn((String)claims.get("orgnmCtfctnUseYn"))
//                .cpsbtDvsCd((String)claims.get("cpsbtDvsCd"))
//                .mmbrCtfctnYn((String)claims.get("mmbrCtfctnYn"))
//                .frnchsMmbrId((String)claims.get("frnchsMmbrId"))
//                .adminId((String)claims.get("adminId"))
//                .authToken(token)
//                .build();
//
//    }

}
