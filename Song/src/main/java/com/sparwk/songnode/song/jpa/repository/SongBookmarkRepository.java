package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongBookmark;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SongBookmarkRepository extends JpaRepository<SongBookmark, Long> {
    Long deleteSongBookmarksBySongId(Long songId);
    Long deleteSongBookmarksByProfileIdAndSongId(Long profileId, Long songId);
}