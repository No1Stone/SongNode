package com.sparwk.songnode.song.biz.v1.song.dto;

import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditDetailResponse;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.CowriterDetailResponse;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongLyricsLangBaseDTO;
import com.sparwk.songnode.song.jpa.dto.SongMetadataBaseDTO;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongDetailResponse {

    @Schema(description = "song")
    private SongDto songResponse;

    @Schema(description = "북마크 Y/N")
    private String bookmarkYn = CommonCodeConst.YN.N;

    @Schema(description = "song metadata")
    private List<SongMetadataBaseDTO> songMetadataBaseDTOs;

    @Schema(description = "song cowriterResponses")
    private List<CowriterDetailResponse> cowriterResponses;

    @Schema(description = "song credit")
    private List<SongCreditDetailResponse> songCreditResponses;

    @Schema(description = "song file")
    private List<SongFileDto> songFileDtos;

    @Schema(description = "노래 가사")
    private SongLyricsDto songLyricsDto;

    @Schema(description = "songLyricsLangResponse")
    List<SongLyricsLangBaseDTO> songLyricsLangResponse;

}
