package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongLyrics;
import com.sparwk.songnode.song.jpa.repository.dsl.SongLyricsRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongLyricsRepository extends JpaRepository<SongLyrics, Long>, SongLyricsRepositoryDsl {

    List<SongLyrics> findBySongId(Long songId);
    SongLyrics findSongLyricsBySongIdAndSongFileSeq(Long songId, Long songFileSeq);

    Long deleteSongLyricsBySongIdAndSongFileSeq(Long songId, Long songFileSeq);
}
