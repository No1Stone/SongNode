package com.sparwk.songnode.song.biz.v1.songLyrics.dto;

import com.sparwk.songnode.song.biz.v1.common.dto.SongCommonParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongLyricsDto {

    @Schema(description = "songCommonDto")
    private SongCommonParam songCommonParam;

    @Schema(description = "송 아이디")
    private Long songId;
    @Schema(description = "송 songFileSeq")
    private Long songFileSeq;
    @Schema(description = "송 가사")
    private String lyrics;
    @Schema(description = "가사 lyricsComt")
    private String lyricsComt;
    @Schema(description = "가사 음란여부")
    private String explicitContentYn;

}
