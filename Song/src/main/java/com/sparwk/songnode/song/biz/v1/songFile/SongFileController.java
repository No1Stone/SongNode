package com.sparwk.songnode.song.biz.v1.songFile;

import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileAndLyricsResponse;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = Environment.API_VERSION+"/file")
@CrossOrigin("*")
@RequiredArgsConstructor
@Api(tags = "Song Server SongFile")
public class SongFileController {

    private final SongFileDomain songFileDomain;
    private Logger logger = LoggerFactory.getLogger(SongFileDomain.class);

    @ApiOperation(
            value = "Song file List/detail"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "songId", value = "songId", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/lyrics-detail/{songId}")
    public Response<SongFileAndLyricsResponse> selectSongFileList(
            HttpServletRequest req,
            @PathVariable(name = "songId") Long songId
    ) {
        logger.info("##### selectSongFileList List #####");
        return songFileDomain.selectSongFile(songId, req);
    }


    @ApiOperation(
            value = "Song file modify"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/modify")
    public Response<Void> updateSongFile(
            HttpServletRequest req,
            @RequestBody SongFileDto songFileDto
    ) {
        logger.info("##### updateSongFile Modify #####");
        return songFileDomain.modifySongFile(songFileDto ,req);
    }

}