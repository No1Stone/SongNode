package com.sparwk.songnode.song.biz.v1.song.dto;


import com.sparwk.songnode.song.biz.v1.common.dto.SongCommonParam;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongBookmarkRequest {

    @ApiModelProperty(name = "songCommonParam", value = "profileId, songId")
    @Schema(name = "songCommonParam", description = "songCommon parameter")
    private SongCommonParam songCommonParam;

    @ApiModelProperty(name = "bookmarkYn", value = "Y,N", example = "Y,N")
    @Schema(name = "bookmarkYn", description = "songCommon bookmarkYn")
    private String bookmarkYn = CommonCodeConst.YN.N;
}


