package com.sparwk.songnode.song.biz.v1.songCowriter;

import com.sparwk.songnode.song.biz.v1.song.dto.SongVersionRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.*;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.AcceptSplitSheetRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.SplitSheetRequest;
import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongCowriterBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongCowriter;
import com.sparwk.songnode.song.jpa.entity.SongCowriterRole;
import com.sparwk.songnode.song.jpa.entity.SongCowriterSp;
import com.sparwk.songnode.song.jpa.repository.SongCowriterRepository;
import com.sparwk.songnode.song.jpa.repository.SongCowriterRoleRepository;
import com.sparwk.songnode.song.jpa.repository.SongCowriterSpRepository;
import com.sparwk.songnode.song.util.TokenUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongCowriterService {

    private final SongCowriterRepository songCowriterRepository;
    private final SongCowriterRoleRepository songCowriterRoleRepository;
    private final SongCowriterSpRepository songCowriterSpRepository;

    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(SongCowriterService.class);

    private final HttpServletRequest request;

    /**
     * song 참여 회원 리스트
     * @param songId
     * @return
     */
    public List<CowriterResponse> selectSongCowriters(Long songId) {
        List<CowriterResponse> result = songCowriterRepository
                .findSongCowritersBySongId(songId).stream()
                .map(e -> modelMapper.map(e, CowriterResponse.class))
                .collect(Collectors.toList());
        return result;
    }

    /**
     * song 참여 회원 리스트
     * @param songId
     * @return
     */
    public SongCowriterBaseDTO selectSongCowriter(Long songId, Long profileId) {
        logger.info("#### selectSongCowriter ####");
        SongCowriter songCowriter = songCowriterRepository.findSongCowriterBySongIdAndProfileId(songId, profileId);
        SongCowriterBaseDTO result = modelMapper.map(songCowriter, SongCowriterBaseDTO.class);
        return result;
    }


    /**
     * profile modify
     * @param dto
     * @return
     */
    @Transactional
    public Long setProfileCowriter(CowriterProfileRequest dto) {
        logger.info("#### SERVICE :: setProfileCowriter :: IN <<<< {}", dto);

        SongCowriterBaseDTO songCowriterBaseDTO = selectSongCowriter(dto.getSongId(), dto.getProfileId());
        // role, sp에 사용
        Long seq = songCowriterBaseDTO.getSongCowriterSeq();

        Long result = null;
        SongCowriterBaseDTO vo = modelMapper.map(dto, SongCowriterBaseDTO.class);
        vo.setSongCowriterSeq(seq);
        vo.setModUsr(dto.getOwnerProfileId());
        vo.setModDt(LocalDateTime.now());
        songCowriterRepository.updateCowriterInfo(vo);

        if(!StringUtils.isEmpty(dto.getRoleCdList())) {
            songCowriterRoleRepository.deleteSongCowriterRolessBySongCowriterSeq(seq);
            Arrays.stream(dto.getRoleCdList().split(",")).forEach(s -> {
                SongCowriterRole entity = SongCowriterRole.builder()
                        .songCowriterSeq(seq)
                        .roleCd(s)
                        .build();
                songCowriterRoleRepository.save(entity);
            });
        }

        if(!StringUtils.isEmpty(dto.getSpProfileIdList())) {
            songCowriterSpRepository.deleteSongCowriterSpsBySongCowriterSeq(seq);
            Arrays.stream(dto.getSpProfileIdList().split(",")).forEach(s -> {
                SongCowriterSp entity = SongCowriterSp.builder()
                        .songCowriterSeq(seq)
                        .spProfileId(Long.parseLong(s))
                        .build();
                songCowriterSpRepository.save(entity);
            });
        }

        return result;
    }

    /**
     * SONG Cowirter init Save
     * @param cowriterRequest
     * @param songId
     */
    @Transactional
    public void saveSongCowriter(CowriterRequest cowriterRequest, Long songId) {
        logger.info("#### saveSongCowriter ####  >>> cowriterRequest {}, songId {}",cowriterRequest, songId);
        UserInfoDTO userInfoDTO = TokenUtil.getTokenInfo(request);

        try {
            Arrays.stream(cowriterRequest.getProfileList().split(",")).forEach(profileId -> {
                SongCowriter songCowriter = SongCowriter.builder()
                        .songId(songId)
                        .acceptYn(CommonCodeConst.YN.N)
                        .profileId(Long.parseLong(profileId))
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .regDt(LocalDateTime.now())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .modDt(LocalDateTime.now())
                        .build();
                songCowriterRepository.save(songCowriter);
            });
        } catch (Exception e) {
            logger.error("####  saveSongCowriter ERROR ####", e.getMessage());
        }
    }

    /**
     * @return
     */
    public List<CowriterDetailResponse> selectSongCowirterDetailList(Long songId){
        logger.info("##### CowriterDetailResponse ###");
        List<CowriterDetailResponse> cowriterDetailResponse = new ArrayList<>();
        List<SongCowriter> songCowriter = songCowriterRepository.findSongCowritersBySongId(songId);
        if(songCowriter.size() > 0) {
            for (SongCowriter cowriter : songCowriter) {
                CowriterDetailResponse result = selectSongCowirterDetail(cowriter.getSongId(), cowriter.getProfileId());
                if(result != null){
                    cowriterDetailResponse.add(result);
                }
            }
        }
        return cowriterDetailResponse;
    }


    /**
     * cowriter detail
     * @param songId
     * @param profileId
     * @return
     */
    public CowriterDetailResponse selectSongCowirterDetail(Long songId, Long profileId){
        logger.info("##### CowriterDetailResponse ###");

        CowriterDetailResponse cowriterDetailResponse = new CowriterDetailResponse();
        SongCowriter songCowriter = songCowriterRepository.findSongCowriterBySongIdAndProfileId(songId, profileId);

        StringBuilder roleList = new StringBuilder();
        StringBuilder spList = new StringBuilder();
        if(songCowriter != null) {
            List<SongCowriterRole> roles = songCowriterRoleRepository.findSongCowriterRolesBySongCowriterSeq(songCowriter.getSongCowriterSeq());
            roles.forEach(songCowriterRole -> {
                roleList.append(songCowriterRole.getRoleCd().trim() + ",");
            });

            List<SongCowriterSp> sps = songCowriterSpRepository.findSongCowriterSpsBySongCowriterSeq(songCowriter.getSongCowriterSeq());
            sps.forEach(songCowriterSp -> {
                spList.append(songCowriterSp.getSpProfileId().toString().trim() + ",");
            });

            cowriterDetailResponse.setSongCowriterResponse(modelMapper.map(songCowriter, SongCowriterBaseDTO.class));
            cowriterDetailResponse.setRoleCdList(roleList.toString());
            cowriterDetailResponse.setSpProfileId(spList.toString());
        }

        return cowriterDetailResponse;
    }


    /**
     * cowriterCopy 작업자 copy
     * @param vo
     */
    @Transactional
    public void cowriterCopy(SongVersionRequest vo){
        logger.info("#### song cowriterCopy ####");

        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        List<CowriterResponse> coList = selectSongCowriters(vo.getSongId());

        if(coList.size() > 0) {
            coList.stream().forEach(e -> {
                SongCowriter songCowriter = SongCowriter.builder()
                        .songId(vo.getNewSongId())
                        .acceptYn(e.getAcceptYn())
                        .profileId(e.getProfileId())
                        .rateShare(0.0)
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .regDt(LocalDateTime.now())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .modDt(LocalDateTime.now())
                        .build();
                songCowriterRepository.save(songCowriter);
            });
        }
    }

    @Transactional
    public Long deleteCowriter(CowriterRequest cowriterRequest) {
        logger.info("#### SERVICE :: deleteCowirter ####");
        Long songId = cowriterRequest.getSongId();
        Long profileId = Long.parseLong(cowriterRequest.getProfileList().replace(",","").trim());
        Long result = songCowriterRepository.deleteSongCowriterBySongIdAndProfileId(songId, profileId);
        return result;
    }

    @Transactional
    public Long deleteCowriterSp(CowriterRequest cowriterRequest) {
        logger.info("#### SERVICE :: deleteCowriterSp ####");
        Long result = songCowriterSpRepository.deleteSongCowriterSpsBySongCowriterSeq(cowriterRequest.getSongCowriterSeq());
        return result;
    }

    @Transactional
    public Long deleteCowriterRole(CowriterRequest cowriterRequest) {
        logger.info("#### SERVICE :: deleteCowriterRole ####");
        Long result = songCowriterRoleRepository.deleteSongCowriterRolessBySongCowriterSeq(cowriterRequest.getSongCowriterSeq());
        return result;
    }

    @Transactional
    public void deleteCowriterAll(Long songId){
        logger.info("#### deleteCowriterAll");
        List<CowriterResponse> list = selectSongCowriters(songId);
        if(list.size() > 0){
            list.stream().forEach(vo -> {
                songCowriterSpRepository.deleteSongCowriterSpsBySongCowriterSeq(vo.getSongCowriterSeq());
                songCowriterRoleRepository.deleteSongCowriterRolessBySongCowriterSeq(vo.getSongCowriterSeq());
                songCowriterRepository.deleteSongCowriterBySongIdAndProfileId(songId, vo.getProfileId());
            });
        }
    }

    /**
     * ####################################################################################################
     *                  SPLIT SHEET
     * ####################################################################################################
     */

    /**
     * share split sheet
     * @param splitSheetRequest
     * @return
     */
    @Transactional
    public void rateShareSplitSheet(SplitSheetRequest splitSheetRequest){
        logger.info("#### SERVICE:: SplitSheetRequest IN <<<<<< {}", splitSheetRequest);
        // song owner인지 체크 필요

        if(splitSheetRequest.getCowriterList().size() > 0 ) {
            splitSheetRequest.getCowriterList().stream().forEach(vo -> {
                SongCowriterBaseDTO songCowriterBaseDTO = SongCowriterBaseDTO.builder()
                        .songId(splitSheetRequest.getSongId())
                        .profileId(vo.getProfileId())
                        .rateShare(vo.getRateShare())
                        .modUsr(splitSheetRequest.getOwnerProfileId())
                        .modDt(LocalDateTime.now())
                        .build();
                songCowriterRepository.updateRateShareSplitSheet(songCowriterBaseDTO, null);
            });
        }
    }

    @Transactional
    public void resetRateShareSplitSheet(Long songId, Long ownerProfileId){
        logger.info("#### resetRateShareSplitSheet #### IN <<<< songId {}, ownerProfileId {} ", songId, ownerProfileId);
        // song owner인지 체크 필요
        SongCowriterBaseDTO songCowriterBaseDTO = SongCowriterBaseDTO.builder()
                .songId(songId)
                .rateShare(Double.valueOf(0.0))
                .acceptYn(CommonCodeConst.YN.N)
                .acceptDt(null)
                .modDt(LocalDateTime.now())
                .modUsr(ownerProfileId)
                .build();
        songCowriterRepository.updateRateShareSplitSheet(songCowriterBaseDTO, CommonCodeConst.MODIFY.RESET);
    }


    /**
     * 스플릿 시트 수락/거부
     * @param acceptSplitSheetRequest
     */
    @Transactional
    public void acceptSplitSheet(AcceptSplitSheetRequest acceptSplitSheetRequest) {
        logger.info("#### acceptSplitSheet #### IN <<<< acceptSplitSheetRequest {} ",acceptSplitSheetRequest);
        SongCowriterBaseDTO songCowriterBaseDTO = SongCowriterBaseDTO.builder()
                .songId(acceptSplitSheetRequest.getSongId())
                .profileId(acceptSplitSheetRequest.getProfileId())
                .acceptYn(acceptSplitSheetRequest.getAcceptYn())
                .rateShareComt(acceptSplitSheetRequest.getRateShareComt())
                .acceptDt(LocalDateTime.now())
                .modDt(LocalDateTime.now())
                .modUsr(acceptSplitSheetRequest.getProfileId())
                .build();
        songCowriterRepository.updateAcceptSplitSheet(songCowriterBaseDTO);

    }


    /**
     *
     * @param songId
     * @return
     */
    @Transactional
    public SongCowriterDto songAvailableCheck(Long songId){
        logger.info("##### countAcceptYn IN <<<<< {} ", songId );
//        Long result = songCowriterRepository.countBySongIdAndAcceptYn(songId, CommonCodeConst.YN.N);
        Long count = songCowriterRepository.findSongCowritersBySongId(songId).stream()
                .filter(songCowriter -> StringUtils.equals(songCowriter.getAcceptYn(), CommonCodeConst.YN.N))
                .count();
        SongCowriterDto result = null;
        if(count < 1) {
            SongCowriter songCowriter = songCowriterRepository.findSongCowritersBySongId(songId).stream()
                    .sorted(Comparator.comparing(SongCowriter::getAcceptDt).reversed())
                    .limit(1).collect(Collectors.toList()).get(0);
            result = modelMapper.map(songCowriter, SongCowriterDto.class);

        }

        logger.debug("#### result count {}",result);
        return result;
    }
}
