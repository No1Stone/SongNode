package com.sparwk.songnode.song.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataBaseDTO {

    private Long songId;
    private String attrTypeCd;
    private String attrDtlCd;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
