package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.Song;
import com.sparwk.songnode.song.jpa.repository.dsl.SongRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface SongRepository extends JpaRepository<Song, Long>, SongRepositoryDsl {
    @Query(value = "SELECT nextval('tb_song_seq')", nativeQuery = true)
    Long newSongSeq();

    Song findSongBySongIdAndSongAvailYn(Long songId, String availYn);

    Song findSongBySongId(Long songId);

}
