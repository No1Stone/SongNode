package com.sparwk.songnode.song.biz.v1.song.dto;


import io.swagger.annotations.ApiModelProperty;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongModifyRequest {

    @ApiModelProperty(name = "originalSongId", value = "originalSongId", required = true)
    private String originalSongId;
    @ApiModelProperty(name = "songTitle", value = "title")
    private String songTitle;
    @ApiModelProperty(name = "songSubTitle", value = "subtitle")
    private String songSubTitle;
    @ApiModelProperty(name = "description", value = "description")
    private String description;
    @ApiModelProperty(name = "avatarImgUseYn", value = "avatar Image Use Yn")
    private String avatarImgUseYn;
    @ApiModelProperty(name = "avatarFileUrl", value = "avatarFileUrl")
    private String avatarFileUrl;

}


