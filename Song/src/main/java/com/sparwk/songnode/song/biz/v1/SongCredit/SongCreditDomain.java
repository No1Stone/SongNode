package com.sparwk.songnode.song.biz.v1.SongCredit;

import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditProfileRequest;
import com.sparwk.songnode.song.biz.v1.SongCredit.dto.SongCreditRequest;
import com.sparwk.songnode.song.biz.v1.timeline.SongTimelineService;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import com.sparwk.songnode.song.config.filter.responsepack.ResultCodeConst;
import com.sparwk.songnode.song.constants.CommonCodeConst;
import com.sparwk.songnode.song.jpa.dto.SongTimelineBaseDTO;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SongCreditDomain {

    private final SongCreditService songCreditService;
    private final SongTimelineService songTimelineService;
    private Logger logger = LoggerFactory.getLogger(SongCreditDomain.class);
    private final MessageSourceAccessor messageSourceAccessor;

    /**
     * Song saveCredit
     * @param songCreditRequest
     * @return
     */
    public Response<Void> saveCredit(SongCreditRequest songCreditRequest){
        logger.info("#### DOMAIN ::: saveCredit ####  IN <<<< {}}", songCreditRequest);
        Response<Void> res = new Response<>();
        songCreditService.saveSongCredit(songCreditRequest, songCreditRequest.getSongId());
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * song cowriter delete
     * @param songCreditRequest
     * @return
     */
    public Response<Void> deleteCowriter(SongCreditRequest songCreditRequest){
        logger.info("#### DOMAIN :: deleteCowriter #### IN <<<< {}", songCreditRequest);
        Response<Void> res = new Response<>();

        songCreditService.deleteSongCredit(songCreditRequest);

        // insertTimeline
        songTimelineService.insertSongTimeline(SongTimelineBaseDTO.builder()
                .songId(songCreditRequest.getSongId())
                .componetCd(CommonCodeConst.SONG_TIMELINE_CODE.SONG)
                .resultMsg("The member has been excluded from the credit.")
                .build());
        // cowriter delete
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    /**
     * credit set profile
     * @param songCreditProfileRequest
     * @return
     */
    public Response<Void> setProfileCredit(SongCreditProfileRequest songCreditProfileRequest) {
        logger.info("#### DOMAIN :: setProfileCredit #### IN <<<< {}", songCreditProfileRequest);
        Response<Void> res = new Response<>();
        songCreditService.setProfileCredit(songCreditProfileRequest);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
