package com.sparwk.songnode.song.biz.v1.songCowriter;

import com.sparwk.songnode.song.biz.v1.songCowriter.dto.*;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.AcceptSplitSheetRequest;
import com.sparwk.songnode.song.biz.v1.songCowriter.dto.split.SplitSheetRequest;
import com.sparwk.songnode.song.config.common.Environment;
import com.sparwk.songnode.song.config.filter.responsepack.Response;
import io.swagger.annotations.*;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = Environment.API_VERSION + "/cowriter")
@CrossOrigin("*")
@Api(tags = "Song Cowriter Server")
@RequiredArgsConstructor
public class SongCowriterController {

    private final SongCowriterDomain songCowriterDomain;
    private final Logger logger = LoggerFactory.getLogger(SongCowriterController.class);



    @ApiOperation(
            value = "Song cowriter Detail"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "songId", value = "Song ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "profileId", value = "Profile ID", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @GetMapping(path = "/detail/{songId}/{profileId}")
    public Response<CowriterDetailResponse> selectSongCowriterDetail(
            @PathVariable(name = "songId") Long songId,
            @PathVariable(name = "profileId") Long profileId
    ) {
        logger.info("#### selectSongList ####");
        return songCowriterDomain.selectSongCowriterDetail(songId ,profileId);
    }


    /**
     * TODO: project-song에 참여한 멤버중 cowriter 추가
     * 그렇지 않은 사람은 project RSVP부터 시작
     */
    @ApiOperation(
            value = "Song cowriter insert"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/save")
    public Response<Void> saveSongCowirters(
            @RequestBody CowriterRequest cowriterRequest
    ) {
        logger.info("#### saveSongCowirters ####");
        return songCowriterDomain.saveCowriter(cowriterRequest);
    }

    /**
     * TODO: project-song에 참여한 멤버중 cowriter 추가
     * 그렇지 않은 사람은 project RSVP부터 시작
     */
    @ApiOperation(
            value = "Song cowriter delete"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/delete")
    public Response<Void> deleteSongCowriter(
            @RequestBody CowriterRequest cowriterRequest
    ) {
        logger.info("#### saveSongCowirters ####");
        return songCowriterDomain.deleteCowriter(cowriterRequest);
    }



    @ApiOperation(
            value = "Song cowriter set profile"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/set/profile")
    public Response<Void> setProfileCowriter(
            @RequestBody CowriterProfileRequest cowriterProfileRequest
    ) {
        logger.info("#### setProfileCowriter #### ,{}", cowriterProfileRequest);
        return songCowriterDomain.setProfileCowriter(cowriterProfileRequest);
    }



    /**
     * TODO: split delete
     * TODO: split reset = delete?
     * TODO: split accptYn = split sheet YN
     * TODO: profile modify = setProfile
     * 시나리오:
     *  ## Song Owner 권한
     *    - set profile
     *    - share split
     *
     */


    /**
     * Split Sheet 분배
     */
    @ApiOperation(
            value = "Song Split Sheet 지분율 분배"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/split-sheet")
    public Response<Void> shareSplitSheet(
            @RequestBody SplitSheetRequest splitSheetRequest
    ) {
        logger.info("#### shareSplitSheet #### IN <<<< {} ");
        return songCowriterDomain.rateShareSplitSheet(splitSheetRequest);
    }


    /**
     * Split Sheet reset
     */
    @ApiOperation(
            value = "Song Split Sheet reset"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "songId", value = "Song ID", dataType = "Long", paramType = "path", required = true),
            @ApiImplicitParam(name = "ownerProfileId", value = "ownerProfileId ID", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/reset/split-sheet/{songId}/{ownerProfileId}")
    public Response<Void> shareSplitSheet(
            @PathVariable(name = "songId") Long songId,
            @PathVariable(name = "ownerProfileId") Long ownerProfileId
    ) {
        logger.info("#### shareSplitSheet #### IN <<<< songId {}, ownerProfileId {} ", songId, ownerProfileId);
        return songCowriterDomain.resetRateShareSplitSheet(songId,ownerProfileId);
    }


    @ApiOperation(
            value = "Song Split Accept"
    )
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
    })
    @PostMapping(path = "/accept/split-sheet")
    public Response<Void> acceptSplitSheet(
            @RequestBody AcceptSplitSheetRequest acceptSplitSheetRequest
    ) {
        logger.info("#### acceptSplitSheet #### IN <<<< acceptSplitSheetRequest {} ", acceptSplitSheetRequest);
        return songCowriterDomain.acceptSplitSheet(acceptSplitSheetRequest);
    }


    @ApiOperation(
            value = "Song Split Accept Count"
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "songId", value = "Song ID", dataType = "Long", paramType = "path", required = true),
    })
    @ApiResponses({
            @ApiResponse(code=0000, message = "성공"),
            @ApiResponse(code=6300, message = "모든 멤버가 accept가 Y가 아님"),
    })
    @GetMapping(path = "/count/accept/{songId}")
    public Response<SongCowriterDto> acceptSplitSheetCount(
            @PathVariable(name = "songId") Long songId
    ) {
        logger.info("#### acceptSplitSheet #### IN <<<< songId {} ", songId);
        return songCowriterDomain.countAcceptCowriter(songId);
    }
}
