package com.sparwk.songnode.song.util;

import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;

public class TokenUtil {

    private final Logger logger = LoggerFactory.getLogger(TokenUtil.class);

    public static void main(String args[]){

//        System.out.println(getSeq(33L));
//        System.out.println(getId("MB"));

    }

    /**
     * Token ProfileId Get
     * @param req
     * @return
     */
    public static UserInfoDTO getTokenInfo(HttpServletRequest req) {
        return (UserInfoDTO) req.getAttribute("UserInfoDTO");
    }

    /**
     * Token ProfileId Get
     * @param req
     * @return
     */
    public static Long getToekenProfileId(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        return userInfoDTO.getLastUseProfileId();
    }

}
