
package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.ProjectSongId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@IdClass(ProjectSongId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_project_song")
public class ProjectSong extends BaseEntity  {

    @Id
    @Column(name = "proj_id", nullable = true)
    private Long projId;

    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;


    @Builder
    ProjectSong(
        Long projId,
        Long songId,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.projId = projId;
        this.songId = songId;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
