package com.sparwk.songnode.song.jpa;

import com.sparwk.songnode.song.jpa.dto.*;
import com.sparwk.songnode.song.jpa.entity.SongCredit;
import com.sparwk.songnode.song.jpa.entity.SongMetadata;
import com.sparwk.songnode.song.jpa.entity.SongMetadataCustom;
import com.sparwk.songnode.song.jpa.repository.*;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class SongServiceBaseRepository {


    final private SongRepository songRepository;
    final private SongCreditRepository songCreditRepository;
    final private SongFileRepository songFileRepository;
    final private SongLyricsRepository songLyricsRepository;
    final private SongMetadataRepository songMetadataRepository;
    final private SongMetadataCustomRepository songMetadataTitleRepository;

    final private SongCowriterRepository projectSongCowriterRepository;
    final private ProjectSongRepository projectSongRepository;

    final private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(SongServiceBaseRepository.class);


    public SongCreditBaseDTO SongCreditSaveService(SongCreditBaseDTO DTO) {
        SongCredit entity = modelMapper.map(DTO, SongCredit.class);
        songCreditRepository.save(entity);
        SongCreditBaseDTO result = modelMapper.map(entity, SongCreditBaseDTO.class);
        return result;
    }

    public SongMetadataBaseDTO SongMetadataSaveService(SongMetadataBaseDTO DTO) {
        SongMetadata entity = modelMapper.map(DTO, SongMetadata.class);
        entity.setRegUsr(Long.valueOf(1));
        entity.setRegDt(LocalDateTime.now());
        entity.setModUsr(Long.valueOf(1));
        entity.setModDt(LocalDateTime.now());
        songMetadataRepository.save(entity);
        SongMetadataBaseDTO result = modelMapper.map(entity, SongMetadataBaseDTO.class);
        return result;
    }

    public SongMetadataCustomDTO SongMetadataTitleSaveService(SongMetadataCustomDTO DTO) {
        SongMetadataCustom entity = modelMapper.map(DTO, SongMetadataCustom.class);
        songMetadataTitleRepository.save(entity);
        SongMetadataCustomDTO result = modelMapper.map(entity, SongMetadataCustomDTO.class);
        return result;
    }


    public List<SongBaseDTO> SongSelectService(Long songId) {
        return songRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongBaseDTO.class)).collect(Collectors.toList());
    }

    public List<SongCreditBaseDTO> SongCreditSelectService(Long songId) {
        return songCreditRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongCreditBaseDTO.class)).collect(Collectors.toList());
    }

    public List<SongFileBaseDTO> SongFileSelectService(Long songId) {
        return songFileRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongFileBaseDTO.class)).collect(Collectors.toList());
    }

    public List<SongLyricsBaseDTO> SongLyricsSelectService(Long songId) {
        return songLyricsRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongLyricsBaseDTO.class)).collect(Collectors.toList());
    }

    public List<SongMetadataBaseDTO> SongMetadataSelectService(Long songId) {
        return songMetadataRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongMetadataBaseDTO.class)).collect(Collectors.toList());
    }

    public List<SongMetadataCustomDTO> SongMetadataTitleSelectService(Long songId) {
        return songMetadataTitleRepository.findById(songId).stream()
                .map(e -> modelMapper.map(e, SongMetadataCustomDTO.class)).collect(Collectors.toList());
    }

}
