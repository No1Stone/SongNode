package com.sparwk.songnode.song.biz.v1.songCowriter.dto.split;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SplitSheetCommonRequest {

    @Schema(name = "profileId")
    private Long profileId;
    @Schema(name = "rateShare")
    private Double rateShare;

}
