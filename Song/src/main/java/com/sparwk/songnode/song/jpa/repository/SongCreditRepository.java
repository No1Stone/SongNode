package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongCredit;
import com.sparwk.songnode.song.jpa.repository.dsl.SongCreditRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCreditRepository extends JpaRepository<SongCredit, Long>, SongCreditRepositoryDsl {

    List<SongCredit> findBySongId(Long songId);
    List<SongCredit> findSongCreditsBySongId(Long songId);

    SongCredit findSongCreditBySongIdAndProfileId(Long songId, Long profileId);
    Long deleteSongCreditBySongCreditSeq(Long songCreditSeq);
}
