package com.sparwk.songnode.song.biz.v1.songLyrics;

import com.sparwk.songnode.song.biz.v1.song.dto.SongVersionRequest;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsLangRequest;
import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import com.sparwk.songnode.song.jpa.dto.SongFileBaseDTO;
import com.sparwk.songnode.song.jpa.dto.SongLyricsBaseDTO;
import com.sparwk.songnode.song.jpa.dto.SongLyricsLangBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongLyrics;
import com.sparwk.songnode.song.jpa.entity.SongLyricsLang;
import com.sparwk.songnode.song.jpa.repository.SongLyricsLangRepository;
import com.sparwk.songnode.song.jpa.repository.SongLyricsRepository;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SongLyricsService {

    private final SongLyricsRepository songLyricsRepository;
    private final SongLyricsLangRepository songLyricsLangRepository;
    private final ModelMapper modelMapper;
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(SongLyricsService.class);

    private final HttpServletRequest request;
    /**
     * 노래 파일에 해당하는 가사 저장
     * @param dto
     * @return
     */
    public SongLyricsBaseDTO songLyricsSaveService(SongLyricsDto dto) {
        SongLyrics entity = modelMapper.map(dto, SongLyrics.class);
        entity.setRegUsr(Long.valueOf(1));
        entity.setModUsr(Long.valueOf(1));
        entity.setRegDt(LocalDateTime.now());
        entity.setModDt(LocalDateTime.now());
        entity = songLyricsRepository.save(entity);
        SongLyricsBaseDTO result = modelMapper.map(entity, SongLyricsBaseDTO.class);
        return result;
    }

    /**
     * 노래에 해당하는 가사 저장
     * @param songId
     * @param songFileSeq
     * @return
     */
    public SongLyricsBaseDTO songLyricsSaveService(Long songId, Long songFileSeq) {
        SongLyricsDto vo = SongLyricsDto.builder()
                .songId(songId)
                .songFileSeq(songFileSeq)
                .build();

        SongLyrics entity = modelMapper.map(vo, SongLyrics.class);
        entity.setRegUsr(Long.valueOf(1));
        entity.setModUsr(Long.valueOf(1));
        entity.setRegDt(LocalDateTime.now());
        entity.setModDt(LocalDateTime.now());
        entity = songLyricsRepository.save(entity);
        SongLyricsBaseDTO result = modelMapper.map(entity, SongLyricsBaseDTO.class);
        return result;
    }

    /**
     * 노래 파일에 해당하는 가사 저장 COPY
     * @param vo
     * @param songLyricsDto
     * @param songFileBaseDTO
     * @return
     */
    public SongLyricsBaseDTO songLyricsCopy(SongVersionRequest vo, SongLyricsDto songLyricsDto, SongFileBaseDTO songFileBaseDTO) {
        SongLyricsBaseDTO result = null;
        if(songLyricsDto != null) {
            songLyricsDto.setSongId(vo.getNewSongId());
            songLyricsDto.setSongFileSeq(songFileBaseDTO.getSongFileSeq());

            SongLyrics entity = modelMapper.map(songLyricsDto, SongLyrics.class);
            entity.setRegUsr(Long.valueOf(1));
            entity.setModUsr(Long.valueOf(1));
            entity.setRegDt(LocalDateTime.now());
            entity.setModDt(LocalDateTime.now());
            entity = songLyricsRepository.save(entity);
            result = modelMapper.map(entity, SongLyricsBaseDTO.class);
        }
        return result;
    }

    /**
     * song file connected songlyrics
     * @param songId
     * @param songFileSeq
     * @return
     */
    public SongLyricsDto songLyricsSelectService(Long songId, Long songFileSeq) {
        SongLyrics songLyrics = songLyricsRepository.findSongLyricsBySongIdAndSongFileSeq(songId, songFileSeq);
        SongLyricsDto result = null;
        if(songLyrics != null){
            result = modelMapper.map(songLyrics, SongLyricsDto.class);
        }
        return result;
    }

    /**
     * song lyrics modify
     * @param dto
     */
    @Transactional
    public void songLyricsDynamicUpdate(SongLyricsDto dto) {
        logger.info("##### songLyricsDynamicUpdate #####");
        try {
            Long result = songLyricsRepository.songLyricsRepositoryDynamicUpdate(dto);
        } catch (Exception e){
            logger.error("#### Error SONG LYRICS MODIFY ERROR ####", e.getMessage());
        }
    }

    @Transactional
    public void deleteSongLyricsAll(Long songId, Long songFileSeq){
        logger.info("####deleteSongLyricsAll ");
        songLyricsRepository.deleteSongLyricsBySongIdAndSongFileSeq(songId, songFileSeq);
    }

    /**
     * select song lang
     * @param songId
     * @return
     */
    public List<SongLyricsLangBaseDTO> selectSongLyricsLang(Long songId){
        logger.info("#### selectSongLyricsLang ##### IN <<<<<< {}", songId);
        List<SongLyricsLangBaseDTO> result = songLyricsLangRepository.findSongLyricsLangsBySongId(songId)
                .stream().map(e -> modelMapper.map(e, SongLyricsLangBaseDTO.class))
                .collect(Collectors.toList());
        logger.info("#### RESULT ##### OUT <<<<<< {}", result);
        return result;
    }

    @Transactional
    public void saveSongLyricsLang(SongLyricsLangRequest dto){
        logger.info("#### saveSongLyricsLang ##### IN <<<<<< {}", dto);
        if(!StringUtils.isEmpty(dto.getLangCdList())){
            Arrays.stream(dto.getLangCdList().split(",")).forEach(s -> {
                SongLyricsLangBaseDTO songLyricsLangBaseDTO = SongLyricsLangBaseDTO.builder()
                        .songId(dto.getSongId())
                        .langCd(s)
                        .regDt(LocalDateTime.now())
                        .modDt(LocalDateTime.now())
                        .regUsr(dto.getProfileId())
                        .modUsr(dto.getProfileId())
                        .build();
                songLyricsLangRepository.save(modelMapper.map(songLyricsLangBaseDTO, SongLyricsLang.class));
            });
        }
    }

    @Transactional
    public void copySongLyricsLang(SongVersionRequest dto) {
        logger.info("#### copySongLyricsLang ##### IN <<<<<< {}", dto);
        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");
        List<SongLyricsLangBaseDTO> list = selectSongLyricsLang(dto.getSongId());
        if(list.size() > 0){
            list.stream().forEach(s -> {
                SongLyricsLang songLyricsLang = SongLyricsLang.builder()
                        .songId(dto.getNewSongId())
                        .langCd(s.getLangCd())
                        .regDt(LocalDateTime.now())
                        .modDt(LocalDateTime.now())
                        .regUsr(userInfoDTO.getLastUseProfileId())
                        .modUsr(userInfoDTO.getLastUseProfileId())
                        .build();
                songLyricsLangRepository.save(songLyricsLang);
            });
        }

    }

    @Transactional
    public void deleteSongLyricsLangAll(Long songId){
        logger.info("#### delete song lang ##### IN <<<<<< {}", songId);
        songLyricsLangRepository.deleteSongLyricsLangsBySongId(songId);
    }

}
