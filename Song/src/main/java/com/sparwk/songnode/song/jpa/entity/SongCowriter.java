
package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.ProjectSongCowriterId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@IdClass(ProjectSongCowriterId.class)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_cowriter")
public class SongCowriter {


    @Id
    @GeneratedValue(generator = "tb_song_cowriter_seq")
    @Column(name = "song_cowriter_seq", nullable = true)
    private Long songCowriterSeq;
    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "rate_share", nullable = true)
    private Double rateShare;
    @Column(name = "accept_yn", nullable = true)
    private String acceptYn;

    @Column(name = "person_id_type", nullable = true)
    private String personIdType;
    @Column(name = "person_id_number", nullable = true)
    private String personIdNumber;
    @Column(name = "op_profile_id", nullable = true)
    private Long opProfileId;
    @Column(name = "pro_profile_id", nullable = true)
    private Long proProfileId;
    @Column(name = "accept_dt", nullable = true)
    private LocalDateTime acceptDt;
    @Column(name = "copyright_control_yn", nullable = true)
    private String copyrightControlYn;
    @Column(name = "rate_share_comt", nullable = true)
    private String rateShareComt;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;


    @Builder
    SongCowriter(
        Long songId,
        Long profileId,
        Double rateShare,
        String acceptYn,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt,
        Long songCowriterSeq,
        String personIdType,
        String personIdNumber,
        Long opProfileId,
        Long proProfileId,
        LocalDateTime acceptDt,
        String copyrightControlYn,
        String rateShareComt
    ) {
        this.songId                 =       songId;
        this.profileId              =       profileId;
        this.rateShare              =       rateShare;
        this.acceptYn               =       acceptYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
        this.songCowriterSeq        =       songCowriterSeq;
        this.personIdType           =       personIdType;
        this.personIdNumber         =       personIdNumber;
        this.opProfileId            =       opProfileId;
        this.proProfileId           =       proProfileId;
        this.acceptDt               =       acceptDt;
        this.copyrightControlYn     =       copyrightControlYn;
        this.rateShareComt          =       rateShareComt;
    }

//    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "proj_id", referencedColumnName = "proj_id",insertable = false,updatable = false)
//    private Project project;

}
