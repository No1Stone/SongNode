package com.sparwk.songnode.song.config.filter.responsepack;

public enum ResultCodeConst {
    //공통처리
    SUCCESS("0000"),
    NOT_FOUND_INFO("0010"),

    //valid
    NOT_VALID_SONG_ID("6000"),
    NOT_EXIST_PROFILE_ID("6010"),
    NOT_FOUND_PROJECT_ID("6020"),
    NOT_VALID_COMPANY_CD("6030"),

    //valid - save
    NOT_REQUIRE_SONG_PARAMETERS("6100"),

    //valid - cowriter
    NOT_REQUIRE_SONG_AVAILABLE_ACCEPT("6300"),

    //Fail
    FAIL("2000"),

    //System Error
    ERROR("9999");
    private String code;

    ResultCodeConst(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
