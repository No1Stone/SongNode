package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongCowriterRole;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongCowriterRoleRepository extends JpaRepository<SongCowriterRole, Long>{
    List<SongCowriterRole> findSongCowriterRolesBySongCowriterSeq(Long songCowriterSeq);

    Long deleteSongCowriterRolessBySongCowriterSeq(Long songCowriterSeq);
}