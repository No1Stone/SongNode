package com.sparwk.songnode.song.jpa.repository;

import com.sparwk.songnode.song.jpa.entity.SongFile;
import com.sparwk.songnode.song.jpa.repository.dsl.SongFileRepositoryDsl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SongFileRepository extends JpaRepository<SongFile, Long>, SongFileRepositoryDsl {

    List<SongFile> findBySongId(Long songId);
    List<SongFile> findSongFilesBySongId(Long songId);

    Long deleteSongFilesBySongIdAndSongFileSeq(Long songId, Long songFileSeq);
}
