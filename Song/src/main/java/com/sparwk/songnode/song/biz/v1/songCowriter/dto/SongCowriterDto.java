
package com.sparwk.songnode.song.biz.v1.songCowriter.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCowriterDto {
    private Long songId;
    private Long profileId;
    private Double rateShare;
    private String acceptYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private Long songCowriterSeq;
    private String personIdType;
    private String personIdNumber;
    private Long opProfileId;
    private Long proProfileId;
    private LocalDateTime acceptDt;
    private String copyrightControlYn;
    private String rateShareComt;
}
