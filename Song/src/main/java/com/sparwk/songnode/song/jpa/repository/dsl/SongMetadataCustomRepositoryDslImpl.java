package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.songnode.song.jpa.entity.SongMetadataCustom;
import com.sparwk.songnode.song.jpa.entity.id.SongMetadataCustomId;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class SongMetadataCustomRepositoryDslImpl implements SongMetadataCustomRepositoryDsl {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(SongMetadataCustomRepositoryDslImpl.class);

    private SongMetadataCustomRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public SongMetadataCustom saveSongMetadataCustom(SongMetadataCustom entity) {
        SongMetadataCustomId songMetadataCustomId = new SongMetadataCustomId();
        songMetadataCustomId.setMetadataSeq(entity.getMetadataSeq());
        songMetadataCustomId.setSongId(entity.getSongId());

        SongMetadataCustom result = em.find(SongMetadataCustom.class, songMetadataCustomId);

        if(result == null){
            entity.setRegUsr(entity.getModUsr());
            entity.setRegDt(LocalDateTime.now());
            em.persist(entity);
            return entity;
        } else {
            em.merge(entity);
        }

        em.flush();
        em.clear();
        return entity;
    }
}
