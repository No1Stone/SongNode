package com.sparwk.songnode.song.biz.v1.timeline;

import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import com.sparwk.songnode.song.jpa.dto.SongTimelineBaseDTO;
import com.sparwk.songnode.song.jpa.entity.SongTimeline;
import com.sparwk.songnode.song.jpa.repository.SongTimelineRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SongTimelineService {

    // song
    private final SongTimelineRepository songTimelineRepository;

    private final HttpServletRequest request;

    private final ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(SongTimelineService.class);


    @Transactional
    public void insertSongTimeline(SongTimelineBaseDTO songTimelineBaseDTO) {
        logger.info("#### SongTimelineService :: TIMELINE IN <<<<<{}", songTimelineBaseDTO);
        UserInfoDTO userInfoDTO = (UserInfoDTO) request.getAttribute("UserInfoDTO");

        songTimelineBaseDTO.setComponentUseId(userInfoDTO.getLastUseProfileId());
        songTimelineBaseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
        songTimelineBaseDTO.setModDt(LocalDateTime.now());
        songTimelineBaseDTO.setRegUsr(userInfoDTO.getLastUseProfileId());
        songTimelineBaseDTO.setRegDt(LocalDateTime.now());

        SongTimeline entity = modelMapper.map(songTimelineBaseDTO, SongTimeline.class);
        songTimelineRepository.save(entity);
    }


    public List<SongTimelineBaseDTO> selectSongTimeline(Long songId){
        logger.info("### timeline ###");
        List<SongTimeline> entityList = songTimelineRepository.findSongTimelinesBySongIdOrderByRegDtDesc(songId);
        List<SongTimelineBaseDTO> result = new ArrayList<>();
        entityList.stream().forEach(e -> {
            SongTimelineBaseDTO vo = SongTimelineBaseDTO.builder()
                    .songId(e.getSongId())
                    .resultMsg(e.getResultMsg())
                    .componetCd(e.getComponetCd())
                    .componentUseId(e.getComponentUseId())
                    .regDt(e.getRegDt())
                    .regUsr(e.getRegUsr())
                    .tlmCd(e.getTlmCd())
                    .build();
            result.add(vo);
        });
        return result;
    }
}
