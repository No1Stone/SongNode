package com.sparwk.songnode.song.biz.v1.projectSong.dto;

public interface ProjectSongCountResponse {

    Long getProjId();
    Long getProfileId();
    Integer getSongCnt();
}
