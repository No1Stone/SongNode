package com.sparwk.songnode.song.config.filter;

import com.google.gson.Gson;
import com.sparwk.songnode.song.config.common.ClientIp;
import com.sparwk.songnode.song.config.filter.token.TokenDecoding;
import com.sparwk.songnode.song.config.filter.token.UserInfoDTO;
import lombok.RequiredArgsConstructor;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
@RequiredArgsConstructor
public class Intercepteradapter implements HandlerInterceptor {

    private final ClientIp clientIp;

    private final TokenDecoding tokenDecoding;


    private final String TOKEN_PREFIX = "Bearer ";

    private final Logger logger = LoggerFactory.getLogger(Intercepteradapter.class);

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {

        if (request.getMethod().equals("OPTIONS")) {
            //CORS인증 유효 처리를 위해
            return true;
        }
        Gson gson = new Gson();
        String reqAuthorization = request.getHeader(HttpHeaders.AUTHORIZATION);
        logger.info("reqAuthorization - {}",reqAuthorization);

        String token = null;
        token = reqAuthorization.substring(TOKEN_PREFIX.length());
        logger.info("token - {}",token);

        if(tokenDecoding.isSigned(token)) {
            String sub = tokenDecoding.getSubject(token);
            UserInfoDTO userInfoDTO = gson.fromJson(sub, UserInfoDTO.class);
            logger.info("userInfoDTO - {}",userInfoDTO);
            request.setAttribute("UserInfoDTO" ,userInfoDTO);
        } else {
//            throw new AuthenticationException(ResultPointCodeConst.AUTH_FAIL.getCode(), "TOKEN AUTH_FAIL");
            logger.error("#### ERROR TOKEN IS ERROR ####");
        }
        return HandlerInterceptor.super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex) throws Exception {
    }

}
