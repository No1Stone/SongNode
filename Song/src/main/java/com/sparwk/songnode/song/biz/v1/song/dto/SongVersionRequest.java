package com.sparwk.songnode.song.biz.v1.song.dto;


import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongVersionRequest {

    @ApiModelProperty(value = "profileId", notes = "profileId" , example = "1")
    @Schema(description = "프로필ID")
    private Long profileId;

    @ApiModelProperty(value = "songId", notes = "SongId" , example = "1")
    @Schema(description = "노래 아이디")
    private Long songId;
    @ApiModelProperty(value = "newSongId", notes = "newSongId" , example = "1")
    @Schema(description = "new song ID")
    private Long newSongId;

    @ApiModelProperty(value = "projId", notes = "projId" , example = "1")
    @Schema(description = "프로젝트 ID")
    private Long projId;

    @ApiModelProperty(value = "newSongFilePath", notes = "새로만든 new song file path" , example = "filePath")
    @Schema(description = "new song filePath")
    private String newSongFilePath;
    @ApiModelProperty(value = "songVersionCd", notes = "노래버전(관리자가 지정한)" , example = "PCODE:SGV")
    @Schema(description = "노래 버전 코드(기본)")
    private String songVersionCd;
    @ApiModelProperty(value = "songVersionCd", notes = "SVG999999,노래버전(사용자지정)" , example = "")
    @Schema(description = "사용자 정의 버전")
    private String userDefineVersion;

}