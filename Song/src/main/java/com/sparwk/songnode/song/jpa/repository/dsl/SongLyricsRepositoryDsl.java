package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.biz.v1.songLyrics.dto.SongLyricsDto;
import org.springframework.transaction.annotation.Transactional;

public interface SongLyricsRepositoryDsl {
    @Transactional
    Long songLyricsRepositoryDynamicUpdate(SongLyricsDto dto);
}
