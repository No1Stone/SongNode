package com.sparwk.songnode.song.jpa.entity;

import com.sparwk.songnode.song.jpa.entity.id.SongMetadataId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_song_metadata")
@IdClass(SongMetadataId.class)
public class SongMetadata extends BaseEntity {

    @Id
    @Column(name = "song_id", nullable = true)
    private Long songId;
    @Id
    @Column(name = "attr_type_cd", nullable = true)
    private String attrTypeCd;

    @Column(name = "attr_dtl_cd", nullable = true)
    @Id
    private String attrDtlCd;

    @Column(name = "reg_usr", updatable = false)
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate
    private LocalDateTime modDt;

    @Builder
    SongMetadata(
            Long songId,
            String attrTypeCd,
            String attrDtlCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.songId = songId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
