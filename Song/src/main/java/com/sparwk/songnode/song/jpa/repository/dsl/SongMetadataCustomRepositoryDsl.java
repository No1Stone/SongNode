package com.sparwk.songnode.song.jpa.repository.dsl;

import com.sparwk.songnode.song.jpa.entity.SongMetadataCustom;

public interface SongMetadataCustomRepositoryDsl {
    SongMetadataCustom saveSongMetadataCustom(SongMetadataCustom entity);
}
