package com.sparwk.songnode.song.biz.v1.songMetadata.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongMetadataCustomRequest {
    @Schema(description = "SongId")
    private Long songId;
    @Schema(description = "ProfileId")
    private Long profileId;
    @Schema(description = "사용자 dto list ")
    List<SongMetadataCustomDto> songMetadataCustomDtoList;
}
