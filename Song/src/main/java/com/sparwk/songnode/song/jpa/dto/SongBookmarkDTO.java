
package com.sparwk.songnode.song.jpa.dto;

import com.sparwk.songnode.song.jpa.entity.BaseEntity;
import com.sparwk.songnode.song.jpa.entity.id.SongBookmarkId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongBookmarkDTO {

    private Long profileId;
    private Long songId;
    private String bookmarkYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
