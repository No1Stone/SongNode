package com.sparwk.songnode.song.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProjectSongCowriterId implements Serializable {
    private Long songCowriterSeq;
    private Long songId;
    private Long profileId;
}
