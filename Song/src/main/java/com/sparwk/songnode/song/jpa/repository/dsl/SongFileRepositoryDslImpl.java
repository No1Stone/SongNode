package com.sparwk.songnode.song.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.songnode.song.biz.v1.songFile.dto.SongFileDto;
import com.sparwk.songnode.song.jpa.entity.QSongFile;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;

@Aspect
public class SongFileRepositoryDslImpl implements SongFileRepositoryDsl {
    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(SongFileRepositoryDslImpl.class);
    private QSongFile qSongFile = QSongFile.songFile;

    private SongFileRepositoryDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long songFileRepositoryDynamicUpdate(SongFileDto entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qSongFile);

        // file name
        if(!StringUtils.isEmpty(entity.getSongFileName())){
            jpaUpdateClause.set(qSongFile.songFileName, entity.getSongFileName());
        }
        // file path
        if(!StringUtils.isEmpty(entity.getSongFilePath())){
            jpaUpdateClause.set(qSongFile.songFilePath, entity.getSongFilePath());
        }
        // file size
        if (entity.getSongFileSize() != null) {
            jpaUpdateClause.set(qSongFile.songFileSize, entity.getSongFileSize());
        }
        // file comment
        if (entity.getSongFileComt() != null) {
            jpaUpdateClause.set(qSongFile.songFileComt, entity.getSongFileComt());
        }
        // bpm
        if (entity.getBpm() != null) {
            jpaUpdateClause.set(qSongFile.bpm, entity.getBpm());
        }
        // contain Sample Yn
        if(!StringUtils.isEmpty(entity.getContainSampleYn())){
            jpaUpdateClause.set(qSongFile.containSampleYn, entity.getContainSampleYn());
        }

        if(entity.getSongCommonParam().getProfileId() != null){
            jpaUpdateClause.set(qSongFile.modUsr, entity.getSongCommonParam().getProfileId());
            jpaUpdateClause.set(qSongFile.modDt, LocalDateTime.now());
        }

        jpaUpdateClause.where(qSongFile.songId.eq(entity.getSongId()));
        jpaUpdateClause.where(qSongFile.songFileSeq.eq(entity.getSongFileSeq()));

        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }
}
