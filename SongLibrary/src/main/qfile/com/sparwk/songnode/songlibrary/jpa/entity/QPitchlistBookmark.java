package com.sparwk.songnode.songlibrary.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPitchlistBookmark is a Querydsl query type for PitchlistBookmark
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPitchlistBookmark extends EntityPathBase<PitchlistBookmark> {

    private static final long serialVersionUID = 1066572380L;

    public static final QPitchlistBookmark pitchlistBookmark = new QPitchlistBookmark("pitchlistBookmark");

    public final StringPath bookmarkYn = createString("bookmarkYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QPitchlistBookmark(String variable) {
        super(PitchlistBookmark.class, forVariable(variable));
    }

    public QPitchlistBookmark(Path<? extends PitchlistBookmark> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPitchlistBookmark(PathMetadata metadata) {
        super(PitchlistBookmark.class, metadata);
    }

}

