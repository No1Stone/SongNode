package com.sparwk.songnode.songlibrary.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPitchlistMetadata is a Querydsl query type for PitchlistMetadata
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPitchlistMetadata extends EntityPathBase<PitchlistMetadata> {

    private static final long serialVersionUID = -1388810155L;

    public static final QPitchlistMetadata pitchlistMetadata = new QPitchlistMetadata("pitchlistMetadata");

    public final StringPath attrDtlCd = createString("attrDtlCd");

    public final StringPath attrTypeCd = createString("attrTypeCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QPitchlistMetadata(String variable) {
        super(PitchlistMetadata.class, forVariable(variable));
    }

    public QPitchlistMetadata(Path<? extends PitchlistMetadata> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPitchlistMetadata(PathMetadata metadata) {
        super(PitchlistMetadata.class, metadata);
    }

}

