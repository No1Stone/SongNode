package com.sparwk.songnode.songlibrary.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPitchlist is a Querydsl query type for Pitchlist
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPitchlist extends EntityPathBase<Pitchlist> {

    private static final long serialVersionUID = 2138896134L;

    public static final QPitchlist pitchlist = new QPitchlist("pitchlist");

    public final StringPath avatarImgPath = createString("avatarImgPath");

    public final StringPath delYn = createString("delYn");

    public final StringPath email = createString("email");

    public final StringPath fullDesc = createString("fullDesc");

    public final StringPath indiviCompYn = createString("indiviCompYn");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath phonNo = createString("phonNo");

    public final StringPath pitchAvatarStyleCd = createString("pitchAvatarStyleCd");

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    public final StringPath pitchStatus = createString("pitchStatus");

    public final StringPath pitchTitle = createString("pitchTitle");

    public final StringPath privatePw = createString("privatePw");

    public final StringPath privateYn = createString("privateYn");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath shareUrl = createString("shareUrl");

    public final StringPath shortDesc = createString("shortDesc");

    public QPitchlist(String variable) {
        super(Pitchlist.class, forVariable(variable));
    }

    public QPitchlist(Path<? extends Pitchlist> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPitchlist(PathMetadata metadata) {
        super(Pitchlist.class, metadata);
    }

}

