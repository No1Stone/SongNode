package com.sparwk.songnode.songlibrary.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QPitchlistMediaInfo is a Querydsl query type for PitchlistMediaInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPitchlistMediaInfo extends EntityPathBase<PitchlistMediaInfo> {

    private static final long serialVersionUID = -1193056692L;

    public static final QPitchlistMediaInfo pitchlistMediaInfo = new QPitchlistMediaInfo("pitchlistMediaInfo");

    public final NumberPath<Long> mediaSeq = createNumber("mediaSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> pitchlistId = createNumber("pitchlistId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath snsTypeCd = createString("snsTypeCd");

    public final StringPath snsUrl = createString("snsUrl");

    public final StringPath useYn = createString("useYn");

    public QPitchlistMediaInfo(String variable) {
        super(PitchlistMediaInfo.class, forVariable(variable));
    }

    public QPitchlistMediaInfo(Path<? extends PitchlistMediaInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QPitchlistMediaInfo(PathMetadata metadata) {
        super(PitchlistMediaInfo.class, metadata);
    }

}

