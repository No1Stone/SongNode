package com.sparwk.songnode.songlibrary.jpa.entity;


import com.sparwk.songnode.songlibrary.jpa.entity.id.PitchlistSongId;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_song")
@IdClass(PitchlistSongId.class)
public class PitchlistSong {
    @Id
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Id
    @Column(name = "song_id", nullable = false)
    private Long songId;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate
    private LocalDateTime regDt;

    @Builder
    PitchlistSong(
            Long pitchlistId,
            Long songId,
            String useYn,
            LocalDateTime regDt
    ) {
        this.pitchlistId = pitchlistId;
        this.songId = songId;
        this.useYn = useYn;
        this.regDt = regDt;
    }

}
