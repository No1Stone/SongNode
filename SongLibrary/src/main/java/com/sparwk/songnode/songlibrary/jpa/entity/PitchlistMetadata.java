package com.sparwk.songnode.songlibrary.jpa.entity;


import com.sparwk.songnode.songlibrary.jpa.entity.id.PitchlistMetadataId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_metadata")
@IdClass(PitchlistMetadataId.class)
public class PitchlistMetadata {
    @Id
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Id
    @Column(name = "attr_type_cd", nullable = true)
    private String attrTypeCd;
    @Id
    @Column(name = "attr_dtl_cd", nullable = true)
    private String attrDtlCd;

    @Column(name = "reg_usr", updatable = false)
    @CreatedBy
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate
    private LocalDateTime modDt;


    @Builder
    PitchlistMetadata(
            Long pitchlistId,
            String attrTypeCd,
            String attrDtlCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.pitchlistId = pitchlistId;
        this.attrTypeCd = attrTypeCd;
        this.attrDtlCd = attrDtlCd;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
