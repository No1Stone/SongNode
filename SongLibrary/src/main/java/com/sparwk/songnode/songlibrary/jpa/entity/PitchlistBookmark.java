package com.sparwk.songnode.songlibrary.jpa.entity;

import com.sparwk.songnode.songlibrary.jpa.entity.id.PitchlistBookmarkId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_bookmark")
@IdClass(PitchlistBookmarkId.class)
public class PitchlistBookmark{

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Id
    @Column(name = "pitchlist_id", nullable = true)
    private Long pitchlistId;
    @Column(name = "bookmark_yn", nullable = true)
    private String bookmarkYn;
    @Column(name = "reg_usr", updatable = false)
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    private LocalDateTime modDt;
    @Builder
    PitchlistBookmark(
            Long profileId,
            Long pitchlistId,
            String bookmarkYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profileId = profileId;
        this.pitchlistId = pitchlistId;
        this.bookmarkYn = bookmarkYn;
        this.regUsr                 =       regUsr;
        this.regDt                  =       regDt;
        this.modUsr                 =       modUsr;
        this.modDt                  =       modDt;
    }

}
