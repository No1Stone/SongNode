package com.sparwk.songnode.songlibrary.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PitchlistMembBaseDTO {
    private Long profileId;
    private Long pitchlistId;
    private String activeYn;
    private String applyStat;
    private String invtStat;
    private LocalDateTime pitchDt;

}
