package com.sparwk.songnode.songlibrary.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class PitchlistMetadataId implements Serializable {
    private Long pitchlistId;
    private String attrTypeCd;
    private String attrDtlCd;
}
