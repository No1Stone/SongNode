package com.sparwk.songnode.songlibrary.biz.v1.pitchlist;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlist.dto.PitchlistRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBaseDTO;
import com.sparwk.songnode.songlibrary.jpa.repository.PitchlistBaseServiceRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PitchlistService {

    @Autowired
    private PitchlistBaseServiceRepository pitchlistBaseServiceRepository;
//    @Autowired
//    private PitchlistRepository PitchlistRepository;
    @Autowired
    private ModelMapper modelMapper;


    public PitchlistBaseDTO PitchlistSaveService(PitchlistRequest dto) {
        PitchlistBaseDTO result = pitchlistBaseServiceRepository
                .PitchlistSaveService(modelMapper.map(dto, PitchlistBaseDTO.class));
        return result;
    }

    public List<PitchlistBaseDTO> PitchlistSelectService(Long profileId){
        List<PitchlistBaseDTO> result =
                pitchlistBaseServiceRepository.PitchlistSelectService(profileId);
        return result;
    }


    public long PitchlistDynamicUpdateService(PitchlistRequest dto) {

        long result = pitchlistBaseServiceRepository
                .PitchlistDynamicUpdateService(modelMapper.map(dto, PitchlistBaseDTO.class));
        return result;
    }

}
