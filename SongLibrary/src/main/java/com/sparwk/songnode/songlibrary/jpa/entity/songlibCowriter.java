package com.sparwk.songnode.songlibrary.jpa.entity;


import lombok.*;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_songlib_cowriter")
public class songlibCowriter {
    @Id
    @GeneratedValue(generator = "tb_songlib_cowriter_seq")
    @Column(name = "songlib_cowriter_seq")
    private Long songlibCowriterSeq;
    @Column(name = "profile_id", nullable = false)
    private Long profileId;
    @Column(name = "cowriter_profile_id", nullable = false)
    private Long cowriterProfileId;
    @Column(name = "cowriter_comp_name", nullable = true)
    private String cowriterCompMame;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate
    private LocalDateTime regDt;

    @Builder
    songlibCowriter(
        Long songlibCowriterSeq,
        Long profileId,
        Long cowriterProfileId,
        String cowriterCompMame,
        LocalDateTime regDt
    ) {
         this.songlibCowriterSeq = songlibCowriterSeq;
         this.profileId = profileId;
         this.cowriterProfileId = cowriterProfileId;
         this.cowriterCompMame = cowriterCompMame;
         this.regDt = regDt;
    }

}
