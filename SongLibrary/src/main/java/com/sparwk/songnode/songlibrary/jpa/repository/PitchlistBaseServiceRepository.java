package com.sparwk.songnode.songlibrary.jpa.repository;

import com.sparwk.songnode.songlibrary.jpa.dto.*;
import com.sparwk.songnode.songlibrary.jpa.entity.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PitchlistBaseServiceRepository {

    @Autowired
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(PitchlistBaseServiceRepository.class);

    @Autowired
    private PitchlistRepository pitchlistRepository;
    @Autowired
    private PitchlistBookmarkRepository pitchlistBookmarkRepository;
    @Autowired
    private PitchlistMediaInfoRepository pitchlistMediaInfoRepository;
    @Autowired
    private PitchlistMetadataRepository pitchlistMetadataRepository;

    public Long PitchlistDynamicUpdateService(PitchlistBaseDTO entity) {
        long result = pitchlistRepository.PitchlistRepositoryDsl(entity);
        return result;
    }

    public Long PitchlistMediaInfoDynamicUpdateService(PitchlistMediaInfoBaseDTO entity) {
        long result = pitchlistMediaInfoRepository.PitchlistMediaInfoRepositoryDslDynamicUpdate(entity);
        return result;
    }

    public PitchlistBaseDTO PitchlistSaveService(PitchlistBaseDTO DTO) {
        Pitchlist entity = modelMapper.map(DTO, Pitchlist.class);
        pitchlistRepository.save(entity);
        PitchlistBaseDTO result = modelMapper.map(entity, PitchlistBaseDTO.class);
        return result;
    }

    public PitchlistBookmarkBaseDTO PitchlistBookmarkSaveService(PitchlistBookmarkBaseDTO DTO) {
        PitchlistBookmark entity = modelMapper.map(DTO, PitchlistBookmark.class);
        pitchlistBookmarkRepository.save(entity);
        PitchlistBookmarkBaseDTO result = modelMapper.map(entity, PitchlistBookmarkBaseDTO.class);
        return result;
    }

    public PitchlistMediaInfoBaseDTO PitchlistMediaInfoSaveService(PitchlistMediaInfoBaseDTO DTO) {
        PitchlistMediaInfo entity = modelMapper.map(DTO, PitchlistMediaInfo.class);
        pitchlistMediaInfoRepository.save(entity);
        PitchlistMediaInfoBaseDTO result = modelMapper.map(entity, PitchlistMediaInfoBaseDTO.class);
        return result;
    }


    public PitchlistMetadataBaseDTO PitchlistMetadataSaveService(PitchlistMetadataBaseDTO DTO) {
        PitchlistMetadata entity = modelMapper.map(DTO, PitchlistMetadata.class);
        pitchlistMetadataRepository.save(entity);
        PitchlistMetadataBaseDTO result = modelMapper.map(entity, PitchlistMetadataBaseDTO.class);
        return result;
    }



    public List<PitchlistBaseDTO> PitchlistSelectService(Long profileId) {
        return pitchlistRepository.findByProfileId(profileId)
                .stream().map(e -> modelMapper.map(e, PitchlistBaseDTO.class)).collect(Collectors.toList());
    }

    public List<PitchlistBookmarkBaseDTO> PitchlistBookmarkSelectService(Long pitchlistId) {
        return pitchlistBookmarkRepository.findByPitchlistId(pitchlistId)
                .stream().map(e -> modelMapper.map(e, PitchlistBookmarkBaseDTO.class)).collect(Collectors.toList());
    }

    public List<PitchlistMediaInfoBaseDTO> PitchlistMediaInfoSelectService(Long pitchlistId) {
        return pitchlistMediaInfoRepository.findByPitchlistId(pitchlistId)
                .stream().map(e -> modelMapper.map(e, PitchlistMediaInfoBaseDTO.class)).collect(Collectors.toList());
    }


    public List<PitchlistMetadataBaseDTO> PitchlistMetadataSelectService(Long pitchlistId) {
        return pitchlistMetadataRepository.findByPitchlistId(pitchlistId)
                .stream().map(e -> modelMapper.map(e, PitchlistMetadataBaseDTO.class)).collect(Collectors.toList());
    }


}
