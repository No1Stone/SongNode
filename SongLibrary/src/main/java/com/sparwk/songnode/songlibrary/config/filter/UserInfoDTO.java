package com.sparwk.songnode.songlibrary.config.filter;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UserInfoDTO {
    private String accountId;
    private List<String> profileId;
}
