package com.sparwk.songnode.songlibrary.biz.v1.pitchlistbookmark;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistbookmark.dto.PitchlistBookmarkRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistBookmarkBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/pitchlistbookmark")
@CrossOrigin("*")
@Api(tags = "PitchlistBookmark Server")
public class PitchlistBookmarkController {

    @Autowired
    private PitchlistBookmarkService pitchlistBookmarkService;
    private final Logger logger = LoggerFactory.getLogger(PitchlistBookmarkController.class);

    @PostMapping(path = "/info")
    public ResponseEntity<PitchlistBookmarkBaseDTO> PitchlistBookmarkSaveController(@Valid @RequestBody PitchlistBookmarkRequest dto) {
        PitchlistBookmarkBaseDTO result = pitchlistBookmarkService.PitchlistBookmarkSaveService(dto);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/info/{pitchlistId}")
    public ResponseEntity<List<PitchlistBookmarkBaseDTO>> PitchlistBookmarkSelect(
            @PathVariable(name = "pitchlistId") Long pitchlistId, HttpServletRequest pi) {
        List<PitchlistBookmarkBaseDTO> result = pitchlistBookmarkService.PitchlistBookmarkSelectService(pitchlistId);
        return ResponseEntity.ok(result);
    }

}
