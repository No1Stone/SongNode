package com.sparwk.songnode.songlibrary.biz.v1.pitchlistmetadata;

import com.sparwk.songnode.songlibrary.biz.v1.pitchlistmetadata.dto.PitchlistMetadataRequest;
import com.sparwk.songnode.songlibrary.jpa.dto.PitchlistMetadataBaseDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = "/V1/pitchlistmeta")
@CrossOrigin("*")
@Api(tags = "PitchlistMetadata Server")
public class PitchlistMetadataController {

    private final Logger logger = LoggerFactory.getLogger(PitchlistMetadataController.class);
    @Autowired
    private PitchlistMetadataService pitchlistMetadataService;

    @PostMapping(path = "/data")
    public ResponseEntity<PitchlistMetadataBaseDTO> PitchlistMetadataSaveController(
            @Valid @RequestBody PitchlistMetadataRequest dto) {
        PitchlistMetadataBaseDTO result = pitchlistMetadataService.PitchlistMetadataSaveService(dto);
        return ResponseEntity.ok(result);
    }

    @GetMapping(path = "/data/{pitchlistId}")
    public ResponseEntity<List<PitchlistMetadataBaseDTO>> PitchlistMetadataSelect(
            @PathVariable(name = "pitchlistId") Long pitchlistId, HttpServletRequest req) {
        List<PitchlistMetadataBaseDTO> result = pitchlistMetadataService.PitchlistMetadataSelectService(pitchlistId);
        return ResponseEntity.ok(result);
    }


}
