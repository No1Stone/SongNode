package com.sparwk.songnode.songlibrary.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class SonglibCowriterSongId implements Serializable {
    private Long songlibCowriterSeq;
    private Long songId;
}
