package com.sparwk.songnode.songlibrary.jpa.entity;

import com.sparwk.songnode.songlibrary.jpa.entity.id.PitchlistAnrId;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_pitchlist_anr")
@IdClass(PitchlistAnrId.class)
public class PitchlistAnr {


    @Id
    @GeneratedValue(generator = "tb_pitchlist_seq")
    @Column(name = "pitchlist_anr_seq")
    private Long pitchlistAnrSeq;
    @Id
    @Column(name = "pitchlist_id")
    private Long pitchlistId;
    @Column(name = "profile_id", nullable = true)
    private Long profileId;

    @Column(name = "pitch_status", nullable = true)
    private String pitchStatus;

    @Column(name = "pitch_yn", nullable = true)
    private String pitchYn;

    @Column(name = "reg_usr", updatable = false)
    @CreatedBy
    private Long regUsr;

    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    private LocalDateTime regDt;

    @Column(name = "mod_usr")
    @LastModifiedBy
    private Long modUsr;

    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    private LocalDateTime modDt;

    @Builder
    PitchlistAnr(
        Long pitchlistAnrSeq,
        Long pitchlistId,
        Long profileId,
        String pitchYn,
        Long regUsr,
        LocalDateTime regDt,
        Long modUsr,
        LocalDateTime modDt
    ) {
        this.pitchlistAnrSeq = pitchlistAnrSeq;
         this.pitchlistId = pitchlistId;
         this.profileId = profileId;
         this.pitchYn = pitchYn;
         this.regUsr                 =       regUsr;
         this.regDt                  =       regDt;
         this.modUsr                 =       modUsr;
         this.modDt                  =       modDt;
    }

}
