# **Development environment configuration**

```
Song Node
```
# **Server Architecture**
```
### sample
Account port: 8001
Profile port: 8002


### song node
Song port : 8105

```

# **Song Project Structure**
```
songNode
- biz(비즈니스)
  - common
  - song
  - songCowriter
  - songCredit
  - songFile
  - songLyrics
  - songMetadata
- config
  - common
  - filter
- constants
- jpa
  - dto
  - entity
  - repository
- util
  - ****
```

# **Song Project DB Update List**
```
## 22.01.13.목
tb_song_cowriter
- ACCEPT_DT
- COPYRIGHT_CONTROL_YN
- RATE_SHARE_COMT
ALTER TABLE tb_song_cowriter ADD COLUMN ACCEPT_DATE timestamp;
ALTER TABLE tb_song_cowriter ADD COLUMN COPYRIGHT_CONTROL_YN  char  default 'N'::bpchar;
ALTER TABLE tb_song_cowriter ADD COLUMN RATE_SHARE_COMT varchar(200);
****
tb_song_cowriter_HIST


```